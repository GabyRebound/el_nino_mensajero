package com.lirirum.nino_mensajero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NinoMensajeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(NinoMensajeroApplication.class, args);
	}

}
